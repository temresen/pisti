﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {
    
    public int number;
    public int type;

    //TYPE SETTINGS - 1 for club, 2 for diamond, 3 for heart, 4 for spade

    void OnMouseDown()
    {
        gameObject.transform.position = new Vector3(0f, 0f, 0f);

        GameController.Instance.current = gameObject;
        GameController.Instance.ground.SetActive(false);
        GameController.Instance.numberofCardsOnGround++;

        // OYUNCU - JOKER ATARSA
        if (gameObject.GetComponent<Card>().number == 11 && GameController.Instance.numberofCardsOnGround > 1)
        {
            // OYUNCU - JOKER İLE PİŞTİ YAPARSA
            if (GameController.Instance.numberofCardsOnGround == 2 && GameController.Instance.ground.GetComponent<Card>().number == 11)
            {
                GameController.Instance.userPoints =+ 20;
                GameController.Instance.userPoints =+ GameController.Instance.pointsUntilNow;
            }
            // OYUNCU - JOKER İLE NORMAL ALIRSA
            else
            {
                GameController.Instance.userPoints =+ GameController.Instance.pointsUntilNow;
            }
            // OYUNCUNUN DESTESİNİN OLDUĞU KISMA AT ÖNCEKİNİ DESTROY ET.
            if (GameController.Instance.lastKnownUser != null)
            {
                Destroy(GameController.Instance.lastKnownUser);
            }
            GameController.Instance.lastKnownUser = gameObject;
            GameController.Instance.current.transform.position = new Vector3(5f, -2f, 0f);
            GameController.Instance.numberofCardsOnGround = 0;
            GameController.Instance.pointsUntilNow = 0;
        }

        // OYUNCU - AYNI SAYIDA ATARSA
        else if ((GameController.Instance.current.GetComponent<Card>().number == GameController.Instance.ground.GetComponent<Card>().number))
        {
            // OYUNCU - AYNI SAYI İLE PİŞTİ YAPARSA
            if (GameController.Instance.numberofCardsOnGround == 2)
            {
                GameController.Instance.userPoints =+ 10; 
                GameController.Instance.userPoints =+ GameController.Instance.pointsUntilNow;
            }
            // OYUNCU - AYNI SAYI İLE NORMAL ALIRSA
            else
            {
                GameController.Instance.userPoints =+ GameController.Instance.pointsUntilNow;

            }
            // OYUNCUNUN DESTESİNE AT ÖNCEKİNİ DESTROY ET.
            if(GameController.Instance.lastKnownUser != null)
            {
                Destroy(GameController.Instance.lastKnownUser);
            }
            GameController.Instance.lastKnownUser = gameObject;
            GameController.Instance.current.transform.position = new Vector3(5f, -2f, 0f);
            GameController.Instance.numberofCardsOnGround = 0;
            GameController.Instance.pointsUntilNow = 0;
        }
        // HİÇBİRİ İSE YERE KOY.
        else
        {
            GameController.Instance.ground = GameController.Instance.current;
            UpdatePoints(gameObject);
        }

        // KULLANILAN CARD I REMOVELA Kİ TEKRAR KULLANILMASIN.
        GameController.Instance.playerCards.Remove(gameObject);
        GameController.Instance.cardList.Remove(gameObject);

        // BİLGİSAYARIN TURU
        StartCoroutine(EnemyPlay());
    }






    IEnumerator EnemyPlay()
    {
        yield return new WaitForSeconds(.75f);

        int a = Random.Range(0, GameController.Instance.computerCards.Count);
        GameController.Instance.numberofCardsOnGround++;
        GameController.Instance.current = null;
        GameController.Instance.ground.SetActive(false);


        GameController.Instance.computerCards[a].transform.position = new Vector3(0f, 0f, 0f);
        GameController.Instance.computerCards[a].SetActive(true);


        // BİLGİSAYAR - JOKER ATARSA
        if (GameController.Instance.computerCards[a].GetComponent<Card>().number == 11 && GameController.Instance.numberofCardsOnGround > 1)
        {
            // BİLGİSAYAR - JOKER İLE PİŞTİ YAPARSA
            if (GameController.Instance.numberofCardsOnGround == 2 && GameController.Instance.ground.GetComponent<Card>().number == 11)
            {
                GameController.Instance.computerPoints = +20;
                GameController.Instance.computerPoints = +GameController.Instance.pointsUntilNow;
            }
            // BİLGİSAYAR - JOKER İLE NORMAL ALIRSA
            else
            {
                GameController.Instance.computerPoints = +10;
                GameController.Instance.computerPoints = +GameController.Instance.pointsUntilNow;
            }
            // BİLGİSAYARIN DESTESİNİN OLDUĞU KISMA AT ÖNCEKİNİ DESTROY ET.
            GameController.Instance.numberofCardsOnGround = 0;
            GameController.Instance.ground.SetActive(false);
            GameController.Instance.computerCards[a].transform.position = new Vector3(-5f, 2f, 0f);
            if (GameController.Instance.lastKnownCmp != null)
            {
                Destroy(GameController.Instance.lastKnownCmp);
            }
            GameController.Instance.lastKnownCmp = GameController.Instance.computerCards[a];
            GameController.Instance.pointsUntilNow = 0;

        }

        // BİLGİSAYAR - AYNI SAYIDA ATARSA
        else if (GameController.Instance.computerCards[a].GetComponent<Card>().number == GameController.Instance.ground.GetComponent<Card>().number)
        {
            // BİLGİSAYAR - AYNI SAYI İLE PİŞTİ YAPARSA
            if(GameController.Instance.numberofCardsOnGround == 2)
            {
                GameController.Instance.computerPoints = +10;
                GameController.Instance.computerPoints = +GameController.Instance.pointsUntilNow;
            }
            // BİLGİSAYAR - AYNI SAYI İLE NORMAL ALIRSA
            else
            {
                GameController.Instance.computerPoints = +GameController.Instance.pointsUntilNow;
            }
            GameController.Instance.numberofCardsOnGround = 0;
            GameController.Instance.ground.SetActive(false);
            GameController.Instance.computerCards[a].transform.position = new Vector3(-5f, 2f, 0f);

            // BİLGİSAYARIN DESTESİNİN OLDUĞU KISMA AT ÖNCEKİNİ DESTROY ET.
            if (GameController.Instance.lastKnownCmp != null)
            {
                Destroy(GameController.Instance.lastKnownCmp);
            }
            GameController.Instance.lastKnownCmp = GameController.Instance.computerCards[a];
            GameController.Instance.pointsUntilNow = 0;
        }

        // HİÇBİRİ İSE YERE KOY.
        else
        {
            GameController.Instance.ground = GameController.Instance.computerCards[a];
            UpdatePoints(GameController.Instance.computerCards[a]);
        }

        // KULLANILAN CARD I REMOVELA Kİ TEKRAR KULLANILMASIN.
        GameController.Instance.computerCards[a].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
        GameController.Instance.computerCards.Remove(GameController.Instance.computerCards[a]);


        if (GameController.Instance.playerCards.Count == 0)
        {
            GameController.Instance.StartGame();
        }
    }

    //YERDEKİ KAĞIDA GÖRE ŞUANA KADAR YERDE OLAN PUANI UPDATE ET.
    public void UpdatePoints(GameObject ground)
    {
        if (ground.GetComponent<Card>().type == 1 && ground.GetComponent<Card>().number == 2)
        {
            GameController.Instance.pointsUntilNow =+ 2;
        }

        if (ground.GetComponent<Card>().type == 2 && ground.GetComponent<Card>().number == 10)
        {
            GameController.Instance.pointsUntilNow =+ 3;
        }

        if (ground.GetComponent<Card>().number == 1 || ground.GetComponent<Card>().number == 11)
        {
            GameController.Instance.pointsUntilNow =+ 1;
        }

        GameController.Instance.cmpPointText.text = "Computer: " + GameController.Instance.computerPoints;
        GameController.Instance.userPointText.text = "User: " + GameController.Instance.userPoints;
    }
}
