﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    public GameObject temp, cards, current, ground, gameOverPanel;
    public Text userPointText, cmpPointText, winnerText; 
    public List<GameObject> cardList = new List<GameObject>();
    public List<GameObject> playerCards = new List<GameObject>();
    public List<GameObject> computerCards = new List<GameObject>();
    public Transform[] spawnPoints;
    public int pointsUntilNow = 0, numberofCardsOnGround = 0, userPoints = 0, computerPoints = 0;
    public GameObject lastKnownUser;
    public GameObject lastKnownCmp;

    private static GameController instance;
    public static GameController Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        cards = GameObject.Find("Cards");
        gameOverPanel.SetActive(false);
        LoadCards();
    }

    //Load All type of cards into cardList as a gameObject.
    void LoadCards()
    { 
        for (int i = 1; i <= 4; i++)
        {
            for (int j = 1; j <= 13; j++)
            {
                temp = Instantiate(Resources.Load("Prefabs/Card") as GameObject);
                temp.name = i + "," + j;
                temp.GetComponent<Card>().type = i;
                temp.GetComponent<Card>().number = j;
                temp.transform.parent = cards.transform;
                switch (i)
                {
                    case 1:
                        temp.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Texture/Cards/Club/club" + j);
                        temp.SetActive(false);
                        break;
                    case 2:
                        temp.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Texture/Cards/Diamond/diamond" + j);
                        temp.SetActive(false);
                        break;
                    case 3:
                        temp.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Texture/Cards/Heart/heart" + j);
                        temp.SetActive(false);
                        break;
                    case 4:
                        temp.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Texture/Cards/Spade/spade" + j);
                        temp.SetActive(false);
                        break;
                }

                cardList.Add(temp);
            }
        }

        StartGame();
        InitialGround();
    }

    public void StartGame()
    {
        if(cardList.Count > 0)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int a = Random.Range(0, cardList.Count);
                    if (i == 0)
                    {
                        playerCards.Add(cardList[a]);
                        cardList[a].transform.position = spawnPoints[j].transform.position;
                        cardList[a].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                        cardList[a].SetActive(true);
                        cardList.Remove(cardList[a]);
                    }
                    else if (i == 1)
                    {
                        cardList[a].transform.position = spawnPoints[j + 4].transform.position;
                        cardList[a].transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
                        cardList[a].SetActive(true);
                        computerCards.Add(cardList[a]);
                        cardList.Remove(cardList[a]);
                    }
                }
            }
        }

        else
        {
            gameOverPanel.SetActive(true);
            if(userPoints > computerPoints)
            {
                winnerText.text = "USER WINS!";
            }
            else if(userPoints < computerPoints)
            {
                winnerText.text = "COMPUTER WINS!";
            }
            else if(userPoints == computerPoints)
            {
                winnerText.text = "DRAW";
            }

        }

    }

    //İLK BAŞTA YERE 4 KAĞIT VER. SON VERDİĞİN AÇIK OLSUN.
    void InitialGround()
    {
        for (int i = 0; i < 4; i++)
        {
            int a = Random.Range(0, cardList.Count);
            if (cardList[a].GetComponent<Card>().type == 1 && cardList[a].GetComponent<Card>().number == 2)
            {
                pointsUntilNow =+ 2;
            }

            if (cardList[a].GetComponent<Card>().type == 2 && cardList[a].GetComponent<Card>().number == 10)
            {
                pointsUntilNow =+ 3;
            }

            if (cardList[a].GetComponent<Card>().number == 10 || cardList[a].GetComponent<Card>().number == 11)
            {
                pointsUntilNow =+ 1;
            }

            if(i == 3)
            {
                cardList[a].transform.position = new Vector3(0f, 0f, 0f);
                ground = cardList[a];
                ground.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
                ground.SetActive(true);
            }
            numberofCardsOnGround++;
            cardList.Remove(cardList[a]);
        } 
    }

    public void onPlayAgainClick()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void onExitClick()
    {
        Application.Quit();
    }
}
